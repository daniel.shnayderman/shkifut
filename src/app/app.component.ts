import { Component, OnInit } from '@angular/core';
import { DataService } from './services/data.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  lists: Observable<any>;

  constructor(private dataService: DataService) {
  }

  ngOnInit(): void {
    this.dataService.getLists().subscribe(result => {
      this.lists = result.years;
    });
  }
  title = 'שקיפות בחינוך';
}
