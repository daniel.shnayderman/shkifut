import { Component, SimpleChanges, Input, OnChanges, AfterViewInit, OnInit, Renderer } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-chart',
  template: '<div [id]="id" ></div>',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  @Input() id: string;
  chart: any;
  //@Input() set options: any;
  @Input()
  set options(options: string) {
    console.log(" chart set options " + this.id);
    try {
         this._options = options;
         if (!this.isFirstTime && options && document.getElementById(this.id)) {
           console.log(" chart set options  Highcharts.chart  " + this.id);
             this.chart = Highcharts.chart(this.id, options);
         }

        }
           catch (error) {
                     console.log(error);
        }
    
  }
  isFirstTime: boolean = true;
  private _options: any;
  constructor() { }

  ngOnInit() {
  
  }
 
  
  ngAfterViewInit(): void {
    console.log(" chart ngAfterViewInit " + this.id);
    if (this.isFirstTime&&this._options && document.getElementById(this.id) ) {
      this.chart = Highcharts.chart(this.id, this._options);
      this.isFirstTime = false;
    }
   
  }
  ngOnChanges(changes: SimpleChanges): void 
    {
  }

  ngOnDestroy() {
    this.chart = null;
  }
}



