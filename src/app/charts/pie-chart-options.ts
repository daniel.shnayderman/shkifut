import * as Highcharts from 'highcharts';
export class PieChartOptions {

  public static pieChartOptions: Highcharts.Options = {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie',
      height: 280,
    },
    title: {
      text: null
    },
    tooltip: {
      pointFormat: '<tr>' +
        '<td style="padding:5px"><b>{point.y}</b></td></tr>',
    },
    plotOptions: {
      pie: {
        innerSize: 150,
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false
        },
        showInLegend: false,
      }
    },
    series: [{
      name: null,
      colorByPoint: true,
      data: null
    }],
    credits: {
      enabled: false
    },
    colors: ['#446F92', '#E8C177', '#6DAC9A', '#24456F'],

    legend: {
      labelFormat: '{name} {percentage:.1f}% - {y} ',
      layout: 'vertical',
      align: 'left',
      // itemMarginBottom: 40,
      lineHeight:50,
      // itemMargintop: 40,
      verticalAlign: 'left',
      x: 0,
      y: 0,
      rtl: true
    },

    //   chart: {
    //     type: 'doughnut',
    //     options3d: {
    //       enabled: true,
    //       alpha: 45,
    //       beta: 0
    //     },
    //     animation: true
    //   },
    //   title: {
    //     text: 'test'
    //   },
    //   credits: {
    //     enabled: false
    //   },
    //   legend: {
    //     labelFormat: '{name} {percentage:.1f}% - ({y})',
    //     layout: 'vertical',
    //     align: 'right',
    //     verticalAlign: 'right',
    //     x: 0,
    //     y: 50,
    //     rtl: true
    //   },
    //   plotOptions: {
    //     pie: {
    //       dataLabels: {
    //         enabled: false
    //       },
    //       depth: 35,
    //       showInLegend: true
    //     }
    //   },
    //   series: [{
    //     name: 'test',
    //     data: null
    //   }]
  };


  public static pieChartOptionsPurpleBlue: Highcharts.Options = {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie',
      height: 230,
    },
    title: {
      text: null
    },
    tooltip: {
      pointFormat: '<tr>' +
        '<td style="padding:5px"><b>{point.y}</b></td></tr>',
    },
    plotOptions: {
      pie: {
        // innerSize: 150,
        innerSize: '75%',
        size: '70%',
        allowPointSelect: true,
        cursor: 'pointer',
        easing: 'easeOutQuad',
        dataLabels: {
          enabled: false
        },
        showInLegend: false
      }
    },
    colors: ['#9971A2', '#78A0D1'],
    credits: {
      enabled: false
    },
  };


}
