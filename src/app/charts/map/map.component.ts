import { Component,OnInit} from '@angular/core';
import { EsriLoaderService } from 'angular-esri-loader';


@Component({
  selector: 'app-map',
  template: `
    <div class="map"><esri-map [webMapProperties]="webMapProperties" [mapViewProperties]="mapViewProperties" (mapInit)="onMapInit($event)"></esri-map></div>`,
  styleUrls: ['./map.component.css'],
  providers: [EsriLoaderService]
})
export class MapComponent implements OnInit {
  webMapProperties: __esri.WebMapProperties = {
    portalItem: {
      id: 'ad5759bf407c4554b748356ebe1886e5'
    }
  };
  mapViewProperties: __esri.MapViewProperties = {
    zoom: 16
  };
  map: __esri.Map;
  mapView: __esri.MapView;

  constructor() { }

  ngOnInit() { }

  onMapInit(mapInfo: { map: __esri.Map, mapView: __esri.MapView }) {
    this.map = mapInfo.map;
    this.mapView = mapInfo.mapView;  
  }
}
