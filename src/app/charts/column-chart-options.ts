
import * as Highcharts from 'highcharts';
export class ColumnChartOptions {
  public static columnChartOptions: Highcharts.Options =
    {
      chart: {
        //    renderTo: 'mem_in_right',
        type: 'column',
      },
      title: {
        text: null
      },
      credits: { enabled: false },
      tooltip: {
        enabled: true,
        pointFormat: '<div  style:"direction:rtl;"><span  style="color:{series.color};padding:0; ">{series.name} </span>' +
          '<span style="padding:0"><b>{point.y:.0f} </b></span> </div>',
        style: {
          fontFamily: '"Assistant", "Arial", sans-serif',
          direction: 'rtl'
        },
        shared: true,
        useHTML: true


      },
      scrollbar: {

      },

      xAxis: {
        categories: null,
        labels: {
          style: {
            fontFamily: '"Assistant", "Arial", sans-serif',
            fontSize: '14px',
            direction: 'rtl',


          },
          useHTML: true,

        },

      },

      yAxis: {
        min: 0,
        gridLineWidth: 0,
        title: {
          text: null
        },
        labels: {
          enabled: true
        },
        stackLabels: {
          enabled: true,

        },

      },
      legend: {
        enabled: false

      },

      plotOptions: {
        column: {
          stacking: 'normal',
          dataLabels: {
            enabled: false
          }
        }
      },
      series: null


    };
}
