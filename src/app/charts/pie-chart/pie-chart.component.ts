import { Component, SimpleChanges, Input, OnChanges, AfterViewInit, OnInit } from '@angular/core';
import { BaseChartComponent } from '../base-chart/base-chart.component';
import * as Highcharts from 'highcharts';


@Component({
  selector: 'pie-chart',
  template: '<div [id]="id"></div>',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit, AfterViewInit, OnChanges {

  @Input() id: string;
  chart: any;
  @Input() data: any;
  defaultData: any = {

    title: {
      text: "שלום"
    },
    credits: {
      enabled: false
    },
    legend: {
      labelFormat: '{name} {percentage:.1f}% - ({y})',
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'right',
      x: 0,
      y: 50,
      rtl: true
    },
    plotOptions: {
      pie: {
        dataLabels: {
          enabled: false
        },
        depth: 35,
        showInLegend: true
      }
    },
    // series: null
    series: [{
      name: "",
      data: null
    }]
  };


  constructor() { }
  ngOnInit() {
    // this.data = Object.assign(this.defaultData, this.data);
  }
  ngAfterViewInit(): void {

  }
  ngOnChanges(changes: SimpleChanges): void {
    if (this.data) {
      this.data = Object.assign(this.defaultData, this.data);

      this.chart = Highcharts.chart(this.id, {
        chart: {
          
          type: 'pie',
          borderRadius: 0,
          height: 230,
        },

        title: {
          text: ''
        },
        tooltip: {
          pointFormat: '<tr><td style="padding:0">{series.name}:</td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        },
        plotOptions: {
          pie: {
            // borderWidth: 6,
            innerSize: '75%',
            size: '70%',
            allowPointSelect: true,
            cursor: 'pointer',
            easing: 'easeOutQuad',
            dataLabels: {
              enabled: false
            },
            showInLegend: false
          }
        },
        colors: ['#9971A2', '#78A0D1'],
        series: [{
          name: this.data.title,
          colorByPoint: true,
          data: this.data.series
        }],
        credits: {
          enabled: false
        },
      })
    }
  }
  chartOptions(): any {
    return {
      chart: {
        type: 'pie',
        options3d: {
          enabled: true,
          alpha: 45,
          beta: 0
        },
        animation: true
      },
      title: {
        text: '',
      },
      credits: {
        enabled: false
      },
      legend: {
        labelFormat: '{name} {percentage:.1f}% - ({y})',
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'right',
        x: 0,
        y: 50,
        rtl: true
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false
          },
          depth: 35,
          showInLegend: true
        }
      },
      series: [{
        name: '',
        data: null
      }]
    };
  }
}
