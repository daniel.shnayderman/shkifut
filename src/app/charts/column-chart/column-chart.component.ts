import { Component, OnInit, AfterViewInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import * as Highcharts from 'highcharts';



@Component({
  selector: 'column-chart',
  template: '<div [id]="id"></div>',
  styleUrls: ['./column-chart.component.css']
})
export class ColumnChartComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() id: string;
  chart: any;
  @Input() data: any;
  defaultData: any = {
    columnData: [{
      name: 'Tokyo',
      data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

    }, {
      name: 'New York',
      data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

    }, {
      name: 'London',
      data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

    }, {
      name: 'Berlin',
      data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

    }],
    // title: { text: 'ממוצע משקעים חודשי' },
    subtitle: { text: 'Source: WorldClimate.com' },
    categories: [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec'
    ],
    yAxis: {
      min: 0,
      title: {
        text: 'Rainfall (mm)'
      }
    },
    legend: {},
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    }
  };
  constructor() { }

  ngOnInit() {
    this.data = Object.assign(this.defaultData, this.data);
  }
  ngOnChanges(changes: SimpleChanges): void {
    //console.log('column-chart ngOnChanges');
    if (this.data) {
      this.data = Object.assign(this.defaultData, this.data);
      this.chart = Highcharts.chart(this.id, {
        chart: {
          type: 'column',
          height: 260,
        },
        credits: {
          enabled: false
        },
        title: this.data.title,
        subtitle: this.data.subtitle,
        xAxis: {
          categories: this.data.categories,
          crosshair: true
        },
        yAxis: this.data.yAxis,
        tooltip: this.data.tooltip,
        plotOptions: {
          column: {
            pointPadding: 0.2,
            borderWidth: 0
          }
        },
        series: [],
        legend: this.data.legend
      });
    }
    if (this.chart) {
      //set categories
      //this.chart.update({
      //  xAxis: {
      //    categories: d.categories
      //  }
      //});
      var seriesLength = this.chart.series.length;
      for (var i = seriesLength - 1; i > -1; i--) {
        this.chart.series[i].remove();
      }
      for (var i = 0; i < this.data.columnData.length; i++) {
        this.chart.addSeries(this.data.columnData[i]);
      }
    }
  }

  ngAfterViewInit(): void {
    //this.chart = Highcharts.chart(this.id, {
    //  chart: {
    //    type: 'column'
    //  },
    //  title: this.data.title,
    //  subtitle: this.data.subtitle,
    //  xAxis: {
    //    categories:this.data.categories,
    //    crosshair: true
    //  },
    //  yAxis: this.data.yAxis,
    //  tooltip: {
    //    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    //    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
    //    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
    //    footerFormat: '</table>',
    //    shared: true,
    //    useHTML: true
    //  },
    //  plotOptions: {
    //    column: {
    //      pointPadding: 0.2,
    //      borderWidth: 0
    //    }
    //  },
    //  series: []
    //});
  }

}
