import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'typeaheadFilter'
})
export class TypeaheadFilterPipe implements PipeTransform {

    transform(value: any[], select: string): any {
        if (select != null && select != undefined) {
            return value.filter(item => item.name.startWith(select));
        }
        return value;
  }

}
