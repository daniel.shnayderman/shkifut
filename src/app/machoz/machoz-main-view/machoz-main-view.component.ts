import { Component, OnInit } from '@angular/core';
import { ViewContextService } from '../../services/viewcontext.service';

@Component({
  selector: 'app-machoz-main-view',
  templateUrl: './machoz-main-view.component.html',
  styleUrls: ['./machoz-main-view.component.css']
})
export class MachozMainViewComponent implements OnInit {

  constructor(public context: ViewContextService) { }

  ngOnInit() {
   
  }

}
