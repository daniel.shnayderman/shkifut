import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms'
import { MachozMainViewComponent } from './machoz-main-view/machoz-main-view.component';
import { GeneralModule } from '../general/general.module';
@NgModule({
  imports: [

    RouterModule.forChild([
      //Leave the route path empty, in order to load the component in the same page (and not in a child page)
      { path: '', component: MachozMainViewComponent }
    ]), CommonModule, FormsModule, GeneralModule


  ],
  providers: [],
  declarations: [MachozMainViewComponent]
})
export class MachozModule { }
