import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ColumnChartComponent } from '../charts/column-chart/column-chart.component';
import { PieChartComponent } from '../charts/pie-chart/pie-chart.component';
import { NgbModule, NgbTypeaheadModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { ChartComponent } from '../charts/chart/chart/chart.component';
@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot()
  ],
  declarations: [ColumnChartComponent, PieChartComponent, ChartComponent],
  exports: [ColumnChartComponent, PieChartComponent, ChartComponent, NgbModule]
})
export class GeneralModule { }
