import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NationalGeneralDataComponent } from './national-general-data.component';

describe('NationalGeneralDataComponent', () => {
  let component: NationalGeneralDataComponent;
  let fixture: ComponentFixture<NationalGeneralDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NationalGeneralDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NationalGeneralDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
