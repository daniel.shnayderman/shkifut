import { Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { DataService } from '../../services/data.service';
import { ViewContextService } from '../../services/viewcontext.service';
import *  as Highcharts from 'highcharts';
import { PieChartOptions } from '../../charts/pie-chart-options';

@Component({
  selector: 'app-national-general-data',
  templateUrl: './national-general-data.component.html',
  styleUrls: ['./national-general-data.component.css']
})
export class NationalGeneralDataComponent implements OnInit {
  @Input() private generalData: any;
  public activeButton: number = 0;
  public avgActiveButton: number = 0;



  ovdeyHoraaGanim: Highcharts.Options;
  TalmidimGanim: Highcharts.Options;
  ovdeyHoraaYesodi: Highcharts.Options;
  TalmidimYesodi: Highcharts.Options;

  halukatMosdotYesodi: Highcharts.Options;
  halukatMosdotGanim: Highcharts.Options;

  firstTime: boolean = true;

  public nationalSchoolData: { title: string, amount: string }[] = [
    { "title": 'מספר מוסדות', "amount": '4,753' },
    { "title": 'מספר תלמידים', "amount": '1,694,021' },
    { "title": 'מספר עובדי הוראה', "amount": '281,142' },
    { "title": 'מספר עובדי הוראה', "amount": '281,142' },
    { "title": 'מספר עובדי הוראה', "amount": '281,142' },
    { "title": 'מספר עובדי הוראה', "amount": '281,142' },
    { "title": 'מספר עובדי הוראה', "amount": '281,142' }];

  constructor(private dataService: DataService, public context: ViewContextService) {
    // this.dataService.getData();

    this.ovdeyHoraaGanim = JSON.parse(JSON.stringify(PieChartOptions.pieChartOptionsPurpleBlue));
    this.ovdeyHoraaYesodi = JSON.parse(JSON.stringify(PieChartOptions.pieChartOptionsPurpleBlue));
    this.TalmidimGanim = JSON.parse(JSON.stringify(PieChartOptions.pieChartOptionsPurpleBlue));
    this.TalmidimYesodi = JSON.parse(JSON.stringify(PieChartOptions.pieChartOptionsPurpleBlue));
    this.halukatMosdotYesodi = JSON.parse(JSON.stringify(PieChartOptions.pieChartOptions));
    this.halukatMosdotGanim = JSON.parse(JSON.stringify(PieChartOptions.pieChartOptions));

  }

  ngOnInit() {

  }



  ngOnChanges(changes: SimpleChanges): void {
    console.log('national-main-view  ngOnChanges');
    if (changes.generalData.currentValue) {
      this.setCharts();

    }
  }

  setCharts() {
    if (this.generalData[0])//to do has to change mock 
      this.generalData = this.generalData[0];
    if (this.generalData.artziKlali) {
      this.setPieChart();
    }
    if (this.generalData.arziKitotShichva) {
      this.setPieForHalukatMosdot();
    }
  }
  setPieChart() {

    this.ovdeyHoraaYesodi.series = [{
      name: "",
      data: [{ name: "מורים", y: this.generalData.artziKlali.MORIM },
      { name: "מורות", y: this.generalData.artziKlali.MOROT }]
    }];
    this.TalmidimYesodi.series = [{
      name: "",
      data: [{ name: "בנים", y: this.generalData.artziKlali.BANIM },
      { name: "בנות", y: this.generalData.artziKlali.BANOT }]
    }];
    this.ovdeyHoraaGanim.series = [{
      name: "",
      data: [{ name: "גננים", y: this.generalData.artziKlali.Gananim },
      { name: "גננות", y: this.generalData.artziKlali.Ganenot }]
    }];
    this.TalmidimGanim.series = [{
      name: "",
      data: [{ name: "בנים", y: this.generalData.artziKlali.Banim_Gan },
      { name: "בנות", y: this.generalData.artziKlali.Banot_Gan }]
    }];

  }
  setPieForHalukatMosdot() {

    this.halukatMosdotYesodi.series[0].data = [
      { name: "יהודי", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_YEHUDI },
      { name: "ערבי", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_ARVI },
      { name: "דרוזי", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_DRUZI },
      { name: "בדואי", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_BEDUI },
      { name: "צ'רקסי", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_CHERKESI }];
    this.halukatMosdotYesodi.series[0].name = "בתי ספר";





  }

  //func(): void {
  //  this.generalData[0].sumClasses[0].KITOT_MAYUCHAD = 4526;
  //}

  setActive(value) {
    this.activeButton = value;
    // this.halukatMosdotYesodi.series[0].data = null;
    this.halukatMosdotYesodi.series[0].data = this.getHalukatMosdotData(value);
  }
  setAvgActive(value){
    this.avgActiveButton = value;
  }
  getHalukatMosdotData(value) {
    switch (value) {
      case 0:
        {
          return [
            { name: "יהודי", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_YEHUDI },
            { name: "ערבי", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_ARVI },
            { name: "דרוזי", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_DRUZI },
            { name: "בדואי", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_BEDUI },
            { name: "צ'רקסי", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_CHERKESI }];

        }
      case 1:
        {
          return [
            { name: "ממלכתי", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_MM },
            { name: "ממלכתי דתי ", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_MMD },
            { name: "חרדי", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_HRD }]
        }
      case 2:
        {
          return [
            { name: "רשמי", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_RISHMI },
            { name: "מוכר", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_MUKAR },
            { name: "פטור", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_PTOR },
            { name: "תרבותי", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_TARBUTI }]
        }
      case 3:
        {
          return [
            { name: "רגיל", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_RAGIL },
            { name: "מיוחד", y: this.generalData.arziKitotShichva.MOSDOT_ARZI_MEYU }]

        }
    }
  }
}
