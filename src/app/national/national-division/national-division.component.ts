import { Component, OnInit, SimpleChanges,  } from '@angular/core';
import *  as Highcharts from 'highcharts';
import { PieChartOptions } from '../../charts/pie-chart-options';
@Component({
  selector: 'app-national-division',
  templateUrl: './national-division.component.html',
  styleUrls: ['./national-division.component.css']
})
export class NationalDivisionComponent implements OnInit {
  halukatMosdotYesodi: Highcharts.Options;
  halukatMosdotGanim: Highcharts.Options;

  constructor() {
    this.halukatMosdotYesodi = JSON.parse(JSON.stringify(PieChartOptions.pieChartOptions));
    this.halukatMosdotGanim = JSON.parse(JSON.stringify(PieChartOptions.pieChartOptions));}

  ngOnInit() {

  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log('NationalDivisionComponent  ngOnChanges');
    if (changes.generalData.currentValue) {
     // this.setCharts();

    }
  }

}
