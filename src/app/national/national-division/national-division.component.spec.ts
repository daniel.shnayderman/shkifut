import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NationalDivisionComponent } from './national-division.component';

describe('NationalDivisionComponent', () => {
  let component: NationalDivisionComponent;
  let fixture: ComponentFixture<NationalDivisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NationalDivisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NationalDivisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
