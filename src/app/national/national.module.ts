import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms'
import { NationalMainViewComponent } from './national-main-view/national-main-view.component';
import { NationalGeneralDataComponent } from './national-general-data/national-general-data.component'
import { NationalBudgetComponent } from './national-budget/national-budget.component';
import { GeneralModule } from '../general/general.module';
import { LocationComponent } from '../components/location/location.component';
import { NationalDivisionComponent } from './national-division/national-division.component';
@NgModule({
  imports: [

    RouterModule.forChild([
      //Leave the route path empty, in order to load the component in the same page (and not in a child page)
      { path: '', component: NationalMainViewComponent }
    ]), CommonModule, FormsModule, GeneralModule

  ],
  providers: [],
  declarations: [
    NationalMainViewComponent,
    NationalGeneralDataComponent,
    NationalBudgetComponent,
    NationalDivisionComponent,
    //  LocationComponent,
  ]
  

})
export class NationalModule { }








