import { Component, OnInit, OnChanges} from '@angular/core';
//ChangeDetectorRef
import { DataService } from '../../services/data.service';
import { ViewContextService } from '../../services/viewcontext.service';

@Component({
  selector: 'app-national-main-view',
  templateUrl: './national-main-view.component.html',
  styleUrls: ['./national-main-view.component.css']
})
export class NationalMainViewComponent implements OnInit  {
  viewContext: any;
  data: any;
  constructor(private dataService: DataService, public context: ViewContextService) {
    console.log('national-main-view constructor');
  }

  ngOnInit() {
    console.log('national-main-view ngOnInit');
 
    this.context.viewContext$.subscribe(viewContext => this.viewContext = viewContext);

    this.dataService.getNationalData(this.viewContext.year).subscribe(result => {
      console.log(result);
      this.data = result;
     // console.log('national-main-view ngOnInit');
    });
  }
}
