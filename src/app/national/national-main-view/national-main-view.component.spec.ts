import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NationalMainViewComponent } from './national-main-view.component';

describe('NationalMainViewComponent', () => {
  let component: NationalMainViewComponent;
  let fixture: ComponentFixture<NationalMainViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NationalMainViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NationalMainViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
