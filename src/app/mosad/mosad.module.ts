import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router';
import { MosadMainViewComponent } from './mosad-main-view/mosad-main-view.component';
import { MosadGeneralDataComponent } from './mosad-general-data/mosad-general-data.component';
import { MosadBudgetComponent } from './mosad-budget/mosad-budget.component';
import { MosadCompareComponent } from './mosad-compare/mosad-compare.component';
import { MapComponent } from '../charts/map/map.component';
import { EsriLoaderModule } from 'angular-esri-loader';
import { AngularEsriModule } from 'angular-esri-components';
import { GeneralModule } from '../general/general.module';
import { MosadSelectComponent } from './mosad-select/mosad-select.component';
import { LocationComponent } from '../components/location/location.component';

@NgModule({
  imports: [RouterModule.forChild([
    //Leave the route path empty, in order to load the component in the same page (and not in a child page)
    { path: '', component: MosadMainViewComponent }
  ]),
    CommonModule,
    FormsModule,
    EsriLoaderModule,
    AngularEsriModule,
    GeneralModule
  ],
  declarations: [
    MosadMainViewComponent,
    MosadGeneralDataComponent,
    MosadBudgetComponent,
    MosadCompareComponent,
    MapComponent,
    MosadSelectComponent,
    LocationComponent
  ],
  // providers: [
  //  { provide: 'BASE_URL', useFactory: getBaseUrl }


  //],
  // bootstrap: [MosadMainViewComponent]
})
export class MosadModule { }


