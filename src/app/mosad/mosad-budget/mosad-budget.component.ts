import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { PieChartOptions } from '../../charts/pie-chart-options';
import { ColumnChartOptions } from '../../charts/column-chart-options';
import *  as Highcharts from 'highcharts';
@Component({
  selector: 'app-mosad-budget',
  templateUrl: './mosad-budget.component.html',
  styleUrls: ['./mosad-budget.component.css']
})
export class MosadBudgetComponent implements OnInit {
  @Input() private generalData: any;


  pieChartOptions: Highcharts.Options;
  columnChartOptions: Highcharts.Options;
  HashvatAlutTalmidOptions: Highcharts.Options;
  HashvatAlutKitaChartOptions: Highcharts.Options;
  HashvatMemuztaTalmidimChartOptions: Highcharts.Options;

  firstTime: boolean = true;
  constructor() {
    this.pieChartOptions = JSON.parse(JSON.stringify(PieChartOptions.pieChartOptions));
    this.columnChartOptions = JSON.parse(JSON.stringify(ColumnChartOptions.columnChartOptions));
  }

  ngOnInit() {
    console.log('mosad-budget ngOnInit');


  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log('mosad-budget ngOnChanges');
    if (changes.generalData.currentValue) {
      this.setCharts();

    }
  }

  setCharts() {
    if (this.generalData[0])//to do has to change mock 
      this.generalData = this.generalData[0];
    if (this.generalData.mosadYearData && this.generalData.mosadMemutzakitotData) {
      this.setPieChart();
      this.setColumnChart();

    }
  }
  ngAfterViewChecked() {

    //if (this.firstTime) {
    //  //this.chartTarget != undefined
    //  if (this.generalData && this.HashvatAlutKita != undefined && this.HashvatAlutTalmid != undefined && this.HashvatMemuztaTalmidim != undefined) {
    //    this.setCharts();
    //    this.firstTime = false;
    //  }
    //}


  }
  setPieChart() {
    this.pieChartOptions.series = [{
      name: "",
      data: [{ name: "שכר מורים", y: this.generalData.mosadYearData.SACHAR_MORIM_MOSAD },
      { name: "תשלומים שאינם שכר מורים", y: this.generalData.mosadYearData.TASHLUMIM_NOSAFIM_MOSAD }]
    }]
 
  }
  setColumnChart() {
 
    this.columnChartOptions.title.text = "עלות תלמיד בבית ספר";
    this.columnChartOptions.xAxis.categories = ["יהודי", "ערבי", "דרוזי", "בדואי"];
    this.columnChartOptions.series = [{
      name: "עלות תלמיד",
      data: [this.generalData.mosadMemutzakitotData.ALOT_TALMID_BIT_SAFAR_YAHODI,
      this.generalData.mosadMemutzakitotData.ALOT_TALMID_BIT_SAFAR_ARAVI, this.generalData.mosadMemutzakitotData.ALOT_TALMID_BIT_SAFAR_DROZI,
      this.generalData.mosadMemutzakitotData.ALOT_TALMID_BIT_SAFAR_BADUYI]
    }];
    this.HashvatAlutTalmidOptions = JSON.parse(JSON.stringify(this.columnChartOptions));

    this.columnChartOptions.title.text = "עלות כיתה בבית ספר";
    this.columnChartOptions.series = [{
      name: "עלות כיתה",
      data: [this.generalData.mosadMemutzakitotData.ALOT_KITA_BIT_SAFAR_YAHODI, this.generalData.mosadMemutzakitotData.ALOT_KITA_BIT_SAFAR_ARAVI,
      this.generalData.mosadMemutzakitotData.ALOT_KITA_BIT_SAFAR_DRUZI, this.generalData.mosadMemutzakitotData.ALOT_KITA_BIT_SAFAR_BEDUYI]
    }];
    this.HashvatAlutKitaChartOptions = JSON.parse(JSON.stringify(this.columnChartOptions));

    this.columnChartOptions.title.text = "ממוצע תלמידים בכיתה";
    this.columnChartOptions.series = [{
      name: "ממוצע תלמידים",
      data: [this.generalData.mosadMemutzakitotData.MEMUZA_TALMID_KITA_YAHODI, this.generalData.mosadMemutzakitotData.MEMUZA_TALMID_KITA_ARAVI,
      this.generalData.mosadMemutzakitotData.MEMUZA_TALMID_KITA_DRUZI, this.generalData.mosadMemutzakitotData.MEMUZA_TALMID_KITA_BEDUYI]
    }];
    this.HashvatMemuztaTalmidimChartOptions = JSON.parse(JSON.stringify(this.columnChartOptions));
 


  }
}







