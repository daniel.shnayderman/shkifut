import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MosadBudgetComponent } from './mosad-budget.component';

describe('MosadBudgetComponent', () => {
  let component: MosadBudgetComponent;
  let fixture: ComponentFixture<MosadBudgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MosadBudgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MosadBudgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
