import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-mosad-general-data',
  templateUrl: './mosad-general-data.component.html',
  styleUrls: ['./mosad-general-data.component.css']
})
export class MosadGeneralDataComponent implements OnInit, OnChanges {

  MosadclassesDistribution: any

  immigrants: any;
  learningDisabilities: any;
  meafyenimClasses: any;
  MMorim: any;
  MTalmidim: any;
  @Input() private generalData: any = [];
  constructor() {
    console.log('mosad-general-data constructor');
  }

  ngOnInit() {
    console.log('mosad-general-data ngOnInit');

  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('mosad-general-data ngOnChanges');

    if (changes.generalData.currentValue) {

      if (this.generalData[0])//to do has to change mock 
        this.generalData = this.generalData[0];

      if (this.generalData.mosadYearData && this.generalData.mosadKitotLeshichvaData) {

        this.setMTalmidim();
        this.setMMorim();
        this.setMeafyenimClasses();
        if (this.generalData.mosadYearData.ACHUZ_HATAMOT_MOSAD) {
          this.setlearningDisabilities();
        }
        if (this.generalData.mosadYearData.TALMIDIM_OLIM) {
          this.setImmigrants();
        }


        this.setMosadClassesDistribution();

      }


    }


  }
  setMTalmidim(): void {
    let series = [
      { name: "בנים", y: this.generalData.mosadYearData.BANIM },
      { name: "בנות", y: this.generalData.mosadYearData.BANOT }
    ]
    this.MTalmidim = {
      series: series,
      title: 'תלמידים'
    }
  }

  setMMorim(): void {
    let series = [
      { name: "מורים", y: this.generalData.mosadYearData.MORIM },
      { name: "מורות", y: this.generalData.mosadYearData.MOROT }
    ]
    this.MMorim = {
      series: series,
      title: 'עובדי הוראה'
    }

  }
  setMeafyenimClasses() {
    let columnData = [
      { showInLegend: false, name: "",
       data: [this.generalData.mosadYearData.SACH_KITOT_MOSAD, this.generalData.mosadYearData.KITOT_LEV_IUNI, this.generalData.mosadYearData.KITOT_LEV_TECHNOLOG, this.generalData.mosadYearData.KITOT_MECHUNANIM
      ],
       color: '#7BB0C0' },

    ]
    this.meafyenimClasses = {
      columnData: columnData,
      // title: { text: 'מאפייני כיתות בבית הספר' },
      subtitle: undefined,
      categories: [
        // 'סה"כ כיתות בי"ס',
        // 'כיתות לב עיוני מב"ר',
        // 'ניתוח לב טכנולוגי אתג"ר',
        // 'כיתת מחוננים'
      ],
      yAxis: {
        min: 0,
        title: undefined,
        tickInterval: 10,
        labels: {
          format: '{value}'
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px;float:right">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0"><b>{point.y:.1f}</b></td><td style="color:{series.color};padding:0;float:right"></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      }
    }
  }
  setlearningDisabilities(): void {
    let columnData = [
      { name: "במוסד", data: [this.generalData.mosadYearData.ACHUZ_HATAMOT_MOSAD], color: '#36a9eb' },
      { name: "בחמישון", data: [this.generalData.mosadYearData.ACHOZ_HATAMOT_HAMESHON], color: '#25e69f' }
    ]
    this.learningDisabilities = {
      columnData: columnData,
      title: { text: 'תלמידי י"ב' },
      subtitle: undefined,
      categories: [
        'שעור התלמידים בכיתה י"ב ברשות המאובחנים כבעלי לקויות למידה שיקבלו התאמות לבחינות בגרות'
      ],
      yAxis: {
        min: 0,
        title: undefined,
        tickInterval: 10,
        labels: {
          format: '{value}%'
        }
      },
      legend: {
        symbolRadius: 0,//Square shape of the legend
        align: 'left',
        verticalAlign: 'top',

      },
      tooltip: {
        headerFormat: '<span style="font-size:10px;float:right">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0"><b>{point.y:.1f}%</b></td><td style="color:{series.color};padding:0;float:right">:{series.name} </td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      }
    }
  }
  setImmigrants(): any {
    let columnData = [
      { showInLegend: false, name: "", data: [this.generalData.mosadYearData.TALMIDIM_OLIM], color: '#808080' },
    ]
    this.immigrants = {
      columnData: columnData,
      title: { text: 'תלמידים עולים' },
      subtitle: undefined,
      categories: [
        'שעור תלמידים עולים בכיתות י-יב'
      ],
      yAxis: {
        min: 0,
        title: undefined,
        tickInterval: 0.1,
        labels: {
          format: '{value}%'
        }
      },
      legend: {
        symbolRadius: 0,//Square shape of the legend
        align: 'left',
        verticalAlign: 'top',

      },
      tooltip: {
        headerFormat: '<span style="font-size:10px;float:right">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0"><b>{point.y:.1f}%</b></td><td style="color:{series.color};padding:0;float:right">:{series.name} </td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      }
    }
  }
  setMosadClassesDistribution(): void {
    let columnData = [
      {
        showInLegend: false, name: "כיתה רגילה", data: [this.generalData.mosadKitotLeshichvaData.KITOT_01_RAGIL,
        this.generalData.mosadKitotLeshichvaData.KITOT_02_RAGIL,
        this.generalData.mosadKitotLeshichvaData.KITOT_03_RAGIL,
        this.generalData.mosadKitotLeshichvaData.KITOT_04_RAGIL,
        this.generalData.mosadKitotLeshichvaData.KITOT_05_RAGIL,
        this.generalData.mosadKitotLeshichvaData.KITOT_06_RAGIL,
        this.generalData.mosadKitotLeshichvaData.KITOT_07_RAGIL,
        this.generalData.mosadKitotLeshichvaData.KITOT_08_RAGIL,
        this.generalData.mosadKitotLeshichvaData.KITOT_09_RAGIL,
        this.generalData.mosadKitotLeshichvaData.KITOT_10_RAGIL,
        this.generalData.mosadKitotLeshichvaData.KITOT_11_RAGIL,
        this.generalData.mosadKitotLeshichvaData.KITOT_12_RAGIL,
        this.generalData.mosadKitotLeshichvaData.KITOT_13_RAGIL,
        this.generalData.mosadKitotLeshichvaData.KITOT_14_RAGIL,
        this.generalData.mosadKitotLeshichvaData.SACH_KITOT_RAGIL,

        ], color: '#7AB0C1'
      },
      {
        showInLegend: false, name: "כיתת חינוך מיוחד", data: [this.generalData.mosadKitotLeshichvaData.KITOT_01_MAYUCHAD,
        this.generalData.mosadKitotLeshichvaData.KITOT_02_MAYUCHAD,
        this.generalData.mosadKitotLeshichvaData.KITOT_03_MAYUCHAD,
        this.generalData.mosadKitotLeshichvaData.KITOT_04_MAYUCHAD,
        this.generalData.mosadKitotLeshichvaData.KITOT_05_MAYUCHAD,
        this.generalData.mosadKitotLeshichvaData.KITOT_06_MAYUCHAD,
        this.generalData.mosadKitotLeshichvaData.KITOT_07_MAYUCHAD,
        this.generalData.mosadKitotLeshichvaData.KITOT_08_MAYUCHAD,
        this.generalData.mosadKitotLeshichvaData.KITOT_09_MAYUCHAD,
        this.generalData.mosadKitotLeshichvaData.KITOT_10_MAYUCHAD,
        this.generalData.mosadKitotLeshichvaData.KITOT_11_MAYUCHAD,
        this.generalData.mosadKitotLeshichvaData.KITOT_12_MAYUCHAD,
          0, 0,
        this.generalData.mosadKitotLeshichvaData.SACH_KITOT_MEYU,
        ], color: '#AEDACD'
      }
    ]
    this.MosadclassesDistribution = {
      columnData: columnData,
      title: {
        text: null
      },
      credits: {
        enabled: false
      },
      subtitle: 'סה"כ כיתות',
      categories: [
        'א',
        'ב',
        'ג',
        'ד',
        'ה',
        'ו',
        'ז',
        'ח',
        'ט',
        'י',
        'יא',
        'יב',
        'יג',
        'יד',
        'סה"כ כיתות'
      ],
      yAxis: {
        max: 20,
        min: 0,
        title: undefined,
        tickInterval: 5,
        labels: {
          format: '{value}'
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px;float:right">{point.key}</span><table>',
        pointFormat: '<tr><td style="padding:0"><b>{point.y:.1f}</b></td><td style="color:{series.color};padding:0;float:right">:{series.name} </td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
    }
  }

}
