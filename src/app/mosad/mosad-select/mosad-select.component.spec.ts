import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MosadSelectComponent } from './mosad-select.component';

describe('MosadSelectComponent', () => {
  let component: MosadSelectComponent;
  let fixture: ComponentFixture<MosadSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MosadSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MosadSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
