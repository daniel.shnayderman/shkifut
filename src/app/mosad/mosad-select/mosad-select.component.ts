import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-mosad-select',
  templateUrl: './mosad-select.component.html',
  styleUrls: ['./mosad-select.component.css']
})
export class MosadSelectComponent implements OnInit {
  @Input() private chart1: any;
  @Input() private chart2: any;
  @Input() private chart3: any;
  @Input() private alutKita: boolean;
  @Output() selectChange: EventEmitter<any> = new EventEmitter();
  select: any;
  
  constructor() { }

  ngOnInit() {
   
  }
 
  onChangeSelect() {

    this.selectChange.emit(this.select);

  }
}
