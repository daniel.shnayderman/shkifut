import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-mosad-compare',
  templateUrl: './mosad-compare.component.html',
  styleUrls: ['./mosad-compare.component.css']
})
export class MosadCompareComponent implements OnInit {
  compearDataYear: any;
  compearData: any;
  @Input() private generalData: any;
  compearStudent: any;
  compearClass: any;
  compearAvg: any;
  compearStudentYear: any;
  compearAvgYear: any;
  compearAvgSimilar: any;
  compearStudentSimilar: any;
  compearDataSimilar: any;
  prop: number;
  value: number;

  constructor() { }
  defualtCompreChart: any = {
    columnData: {},
    title: {},
    subtitle: undefined,
    categories: [],
    yAxis: {
      min: 0,
      title: undefined,
      tickInterval: 10,
      labels: {
        format: '{value}'
      }
    },
    legend: {
      symbolRadius: 0,//Square shape of the legend
      align: 'left',
      verticalAlign: 'top',

    },
    tooltip: {
      headerFormat: '<span style="font-size:10px;float:right">{point.key}</span><table>',
      pointFormat: '<tr><td style="padding:0"><b>{point.y:.1f}</b></td><td style="color:{series.color};padding:0;float:right"></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    }
  }
  ngOnInit() {
    this.prop = 0;
    this.value = 0;
  }
  onChange() {

  }
  ngOnChanges(changes: SimpleChanges): void {

    if (changes.generalData.currentValue) {
      if (this.generalData[0])//to do has to change mock 
        this.generalData = this.generalData[0];
      if (this.generalData.mosadYearData && this.generalData.mosadKitotLeshichvaData) {

        if (this.generalData.mosadRashutData.length > 0) {
          this.setCompearStudent();
          this.setCompearClass();
          this.setCompearAvg();
          this.setCompearStudentYear();
          this.setCompearAvgYear();
        }

      
      
        this.compearDataSimilar = this.setCompearStudentSimilar(this.generalData.mosadSimilarRashut);
      
     
      }
    }
  }
  onChangeSelect(value: any) {
    this.compearData = value;
  }
  onChangeSelectYear(value: any) {
    this.compearDataYear = value;
  }
  onChangeSelectSimilar(value: any) {
    this.value = value;
    this.onChangeProp(this.prop);

  }
  onChangeProp(prop: number) {
    switch (prop) {
      case 0:
        this.compearDataSimilar = this.value == 0 ? this.setCompearStudentSimilar(this.generalData.mosadSimilarRashut) : this.setCompearAvgSimilar(this.generalData.mosadSimilarRashut);
        break;
      case 1:
        this.compearDataSimilar = this.value == 0 ? this.setCompearStudentSimilar(this.generalData.mosadSimilarRashutLeloMigzar) : this.setCompearAvgSimilar(this.generalData.mosadSimilarRashutLeloMigzar);
        break;
      case 2:
        this.compearDataSimilar = this.value == 0 ? this.setCompearStudentSimilar(this.generalData.mosadSimilarHamishon) : this.setCompearAvgSimilar(this.generalData.mosadSimilarHamishon);
        break;
      default:
    }
  }


  //השוואה בין איזורים
  setCompearStudent(): void {
    let columnData = [
      {
        showInLegend: false, name: "", data: [this.generalData.mosadYearData.ALUT_TALMID,
        this.generalData.mosadRashutData.find(x => x.KEY_MEMUZA_RASHUTI === this.generalData.mosadYearData.KEY_MEMUZA_RASHUTI).ALUT_TALMID_RASHUTI,
        this.generalData.mosadMachozData.find(x => x.KEY_MEMUZA_MECHOZI === this.generalData.mosadYearData.KEY_MEMUZA_MECHOZI).ALUT_TALMID_MACHOZ,
        this.generalData.mosadArziData.find(x => x.KEY_MEMUZA_ARZI === this.generalData.mosadYearData.KEY_MEMUZA_ARZI).ALUT_TALMID_ARZI,
        this.generalData.mosadHamishonData.ALUT_TALMID_HAMESHON,
        ], color: '#36a9eb'
      },
    ]
    this.compearStudent = {
      columnData: columnData,
      title: { text: 'השוואת עלות תלמיד בבית ספר' },
      categories: [
        'עלות תלמיד במוסד',
        'עלות תלמיד ברשות',
        'עלות תלמיד במחוז',
        'עלות תלמיד ארצית',
        'עלות תלמיד בחמישות',
      ],
   
    }
    this.compearStudent = this.getObject(this.compearStudent);
    this.compearData = this.compearStudent;

  }
  setCompearClass(): void {
    let columnData = [
      {
        showInLegend: false, name: "", data: [this.generalData.mosadYearData.ALUT_KITA,
        this.generalData.mosadRashutData.find(x => x.KEY_MEMUZA_RASHUTI === this.generalData.mosadYearData.KEY_MEMUZA_RASHUTI).ALUT_KITA_RASHUTI,
        this.generalData.mosadMachozData.find(x => x.KEY_MEMUZA_MECHOZI === this.generalData.mosadYearData.KEY_MEMUZA_MECHOZI).ALUT_KITA_MACHOZ,
        this.generalData.mosadArziData.find(x => x.KEY_MEMUZA_ARZI === this.generalData.mosadYearData.KEY_MEMUZA_ARZI).ALUT_KITA_ARZI,
        this.generalData.mosadHamishonData.ALUT_KITA_HAMESHON,
        ], color: '#36a9eb'
      },
    ]
    this.compearClass = {
      columnData: columnData,
      title: { text: 'השוואת עלות כיתה בבית ספר' },
      categories: [
        'עלות כיתה במוסד',
        'עלות כיתה ברשות',
        'עלות כיתה במחוז',
        'עלות כיתה ארצית',
        'עלות כיתה בחמישות',
      ],

    }
    this.compearClass = this.getObject(this.compearClass);
  }
  setCompearAvg(): void {
    let columnData = [
      {
        showInLegend: false, name: "", data: [this.generalData.mosadYearData.MISPAR_TALMIDIM_BE_KITA,
        this.generalData.mosadRashutData.find(x => x.KEY_MEMUZA_RASHUTI === this.generalData.mosadYearData.KEY_MEMUZA_RASHUTI).MEMUZA_TALMIDIM_KITA_RASHUTI,
        this.generalData.mosadMachozData.find(x => x.KEY_MEMUZA_MECHOZI === this.generalData.mosadYearData.KEY_MEMUZA_MECHOZI).MEMUZA_TALMIDIM_KITA_MECHOZI,
        this.generalData.mosadArziData.find(x => x.KEY_MEMUZA_ARZI === this.generalData.mosadYearData.KEY_MEMUZA_ARZI).MEMUZA_TALMIDIM_KITA_ARZI,
        this.generalData.mosadHamishonData.MEMUZA_TALMIDIM_KITA_HAMISON,
        ], color: '#36a9eb'
      },
    ]
    this.compearAvg = {
      columnData: columnData,
      title: { text: 'השוואת ממוצע תלמידים בכיתה' },
      categories: [
        'גודל כיתה ממוצע במוסד',
        'גודל כיתה ממוצע ברשות',
        'גודל כיתה ממוצע במחוז',
        'גודל כיתה ממוצע ארצית',
        'גודל כיתה ממוצע בחמישות',],
    }
    this.compearAvg = this.getObject(this.compearAvg);
  }


  //השוואה בין שנתית

  setCompearStudentYear(): void {
    let columnData = [
      { name: "מוסד", data: this.generalData.mosadDataHistory.map(x => x.ALUT_TALMID), color: '#36a9eb' },
      { name: "רשות", data: this.generalData.mosadRashutData.map(x => x.ALUT_TALMID_RASHUTI), color: '#4cff00' },
      { name: "מחוז", data: this.generalData.mosadMachozData.map(x => x.ALUT_TALMID_MACHOZ), color: '#38ed94' },
      { name: "ארצי", data: this.generalData.mosadArziData.map(x => x.ALUT_TALMID_ARZI), color: '#99be1a' },]
    this.compearStudentYear = {
      columnData: columnData,
      title: { text: 'השוואת עלות תלמיד בבית ספר' },
      categories: this.generalData.years.years.map(x => x.SHEM_SHANA_LOAZITH),
    }
    this.compearStudentYear = this.getObject(this.compearStudentYear);
    this.compearDataYear = this.compearStudentYear;
  }
  setCompearAvgYear(): void {

    let columnData = [{ name: "מוסד", data: this.generalData.mosadDataHistory.map(x => x.MISPAR_TALMIDIM_BE_KITA), color: '#36a9eb' },
    { name: "רשות", data: this.generalData.mosadRashutData.map(x => x.MEMUZA_TALMIDIM_KITA_RASHUTI), color: '#4cff00' },
    { name: "מחוז", data: this.generalData.mosadMachozData.map(x => x.MEMUZA_TALMIDIM_KITA_MECHOZI), color: '#38ed94' },
    { name: "ארצי", data: this.generalData.mosadArziData.map(x => x.MEMUZA_TALMIDIM_KITA_ARZI), color: '#99be1a' },]
    this.compearAvgYear = {
      columnData: columnData,
      title: { text: 'השוואת ממוצע תלמידים בכיתה' },
      categories: this.generalData.years.years.map(x => x.SHEM_SHANA_LOAZITH),
    }
    this.compearAvgYear = this.getObject(this.compearAvgYear);
  }


  //השוואה בין מוסדות

  setCompearAvgSimilar(data): any {
    let columnData = [{ showInLegend: false, name: "", data: data.map(x => x.MISPAR_TALMIDIM_BE_KITA), color: '#36a9eb' },]
    this.compearAvgSimilar = {
      columnData: columnData,
      title: { text: 'ממוצע תלמידים' },
      categories: data.map(x => x.SHEM_MOSAD),
    }
    this.compearAvgSimilar = this.getObject(this.compearAvgSimilar);
    return this.compearAvgSimilar;
  }

  setCompearStudentSimilar(data): any {
    let columnData = [{ showInLegend: false, name: "", data: data.map(x => x.ALUT_TALMID), color: '#36a9eb' },]
    this.compearStudentSimilar = {
      columnData: columnData,
      title: { text: 'עלות תלמיד' },
      categories: data.map(x => x.SHEM_MOSAD),
    }

    this.compearStudentSimilar = this.getObject(this.compearStudentSimilar);
   return this.compearStudentSimilar;
  }
  getObject(data) {

    var temp = Object.assign({}, this.defualtCompreChart);
    return Object.assign(temp, data);

  }
}
