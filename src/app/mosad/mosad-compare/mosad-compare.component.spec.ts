import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MosadCompareComponent } from './mosad-compare.component';

describe('MosadCompareComponent', () => {
  let component: MosadCompareComponent;
  let fixture: ComponentFixture<MosadCompareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MosadCompareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MosadCompareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
