import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MosadMainViewComponent } from './mosad-main-view.component';

describe('MosadMainViewComponent', () => {
  let component: MosadMainViewComponent;
  let fixture: ComponentFixture<MosadMainViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MosadMainViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MosadMainViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
