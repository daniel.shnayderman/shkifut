import { Component, OnInit } from '@angular/core';
import { ViewContextService } from '../../services/viewcontext.service';
import { DataService } from '../../services/data.service';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-mosad-main-view',
  templateUrl: './mosad-main-view.component.html',
  styleUrls: ['./mosad-main-view.component.css']
})
export class MosadMainViewComponent implements OnInit {
  data: any;
  currentTabIndex: string = 'ngb-tab-4';
  constructor(private dataService: DataService, public context: ViewContextService) {
    console.log('mosad-main-view constructor');

  };

  ngOnInit() {
    console.log('mosad-main-view ngOnInit');
 
    this.context.viewContext$
      .subscribe(viewContext => {
        if (viewContext.semel !== undefined) {
          this.dataService.getMosadData(viewContext.semel, viewContext.year).subscribe(result => {
            this.data = result;

          });
        }
        
      });

  }
  beforeChange($event: NgbTabChangeEvent) {
    
     this.currentTabIndex = $event.nextId;
  //  this.currentTabIndex++;
  }
  //OnMosadChanged(mosadChanged: number): void {
  //  var r = mosadChanged;
  //  this.dataService.getMosadData(mosadChanged, this.context.year).subscribe(result => {
  //          this.data = result;
  //        });
  //}

}
