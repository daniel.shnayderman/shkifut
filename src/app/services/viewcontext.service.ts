import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ViewContextService {

  

  private viewContext = new BehaviorSubject<ViewContext>({
    mode: "national",
    year: 2017,
    semel: 1
  });
  
  constructor() {
  }

  get viewContext$() {
    return this.viewContext.asObservable();
  }

 
  changeViewContext(viewContext: ViewContext) {
    this.viewContext.next(viewContext);
  }

  changeYear(year: number) {
    this.viewContext.value.year = year;
    this.changeViewContext(this.viewContext.value);
   
  }
  changeMode(mode: string) {
    this.viewContext.value.mode = mode;
    this.changeViewContext(this.viewContext.value);
    
  }
}

export class ViewContext {
  mode: string;
  year: number;
  semel: number;
}
