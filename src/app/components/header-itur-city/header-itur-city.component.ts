import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { NgControl } from '@angular/forms';
import { TypeaheadFilterPipe } from "../../pipes/typeahead-filter.pipe";
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Router } from "@angular/router";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'itur-city',
  templateUrl: './header-itur-city.component.html',
  styleUrls: ['./header-itur-city.component.css']
})
export class HeaderIturCityComponent implements OnInit {

  constructor(private router: Router) { }
  @Input() mosdot: any;
  model: any;
  @ViewChild('instance') instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  ngOnInit() {
  }

  search = (text$: Observable<string>) =>
    text$
      .merge(this.focus$)
      //.merge(this.click$.filter(() => !this.instance.isPopupOpen()))
      .map(term => term.length < 2 ? []
        : this.mosdot.filter(a => this.filterfortypeahead(term, a, [
          //{ key: "ISV_RASHUT_CHINUCH", pos: 1 },
          { key: "SHEM_RASHUT", pos: 0 }
        ]) === true));


  //formatter = (x: { SHEM_RASHUT: string, ISV_RASHUT_CHINUCH: number }) => x.SHEM_RASHUT + "(" + x.ISV_RASHUT_CHINUCH.toString() + ")" ;
  formatter = (x: { SHEM_RASHUT: string }) => x.SHEM_RASHUT;

  filterfortypeahead(val, actualState, props) {

    var tempVal = val.replace(/[(|)|,]/g, " ");
    var searchTerms = tempVal.split(" ");
    if (typeof actualState === 'object') {
      for (var p = 0; p < props.length; p++) { //prop
        var key = props[p].key;
        var prefixOnly = (props[p].pos === 0);
        var i = 0;
        while (i < searchTerms.length) { //expr
          var words = actualState[key].toString().split(' ');
          var found = false;
          for (var j = 0; j < words.length; j++) {
            var idx = words[j].indexOf(searchTerms[i]);
            if (idx === 0 || (idx > 0 && !prefixOnly)) {
              found = true;
              searchTerms.splice(i, 1);
              break;
            }
          }
          //if expression not found we look for next one
          if (!found) i++;
        }
      }
    }
    return searchTerms.length === 0;
  };
  onChange() {
    if (this.model) {
      this.model.ISV_RASHUT_CHINUCH ? this.router.navigate(['city', this.model.ISV_RASHUT_CHINUCH]) : this.router.navigate(['city']);
    }

  }

}
