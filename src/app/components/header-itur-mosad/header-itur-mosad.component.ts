import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { NgControl } from '@angular/forms';
import { TypeaheadFilterPipe } from "../../pipes/typeahead-filter.pipe";
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Router } from "@angular/router";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/filter';
import { ViewContextService } from '../../services/viewcontext.service';

@Component({
  selector: 'itur-mosad',
  templateUrl: './header-itur-mosad.component.html',
  styleUrls: ['./header-itur-mosad.component.css']
})
export class HeaderIturMosadComponent implements OnInit {

  constructor(private router: Router, public context: ViewContextService) { }
  @Input() mosdot: any;
  model: any;
  @ViewChild('instance') instance: NgbTypeahead;
  //focus$ = new Subject<string>();
  //click$ = new Subject<string>();
  @Output() mosadChanged: EventEmitter<number> = new EventEmitter();

  ngOnInit() {
    console.log(this.mosdot);
  }

  search = (text$: Observable<string>) =>
    text$
      //.merge(this.focus$)
      //.merge(this.click$.filter(() => !this.instance.isPopupOpen()))
      .map(term => term.length < 2 ? []
        : this.mosdot.filter(a => this.filterfortypeahead(term, a, [
          { key: "SEMEL_MOSAD", pos: 1 },
          { key: "SHEM_MOSAD", pos: 0 },
          { key: "KTOVET_MOSAD", pos: 0 }
        ]) === true).slice(0, 10));


  formatter = (x: { SHEM_MOSAD: string, SEMEL_MOSAD: number, KTOVET_MOSAD: string }) => x.SHEM_MOSAD + "(" + x.SEMEL_MOSAD.toString() + ") ," + x.KTOVET_MOSAD;
 
 
  onChange() {
    if (this.model) {
      //this.model.SEMEL_MOSAD ? this.router.navigate(['school', this.model.SEMEL_MOSAD]) : this.router.navigate(['school']);
    }
    
  }

  searchMosad(): void {
    //this.context.changeSemelMosad(this.model.SEMEL_MOSAD);
    //this.mosadChanged.emit(this.model.SEMEL_MOSAD);
  }

  filterfortypeahead(val, actualState, props) {

      var tempVal = val.replace(/[(|)|,]/g, " ");
      var searchTerms = tempVal.split(" ");
      if (typeof actualState === 'object') {
        for (var p = 0; p < props.length; p++) { //prop
          var key = props[p].key;
          var prefixOnly = (props[p].pos === 0);
          var i = 0;
          while (i < searchTerms.length) { //expr
            var words = actualState[key].toString().split(' ');
            var found = false;
            for (var j = 0; j < words.length; j++) {
              var idx = words[j].indexOf(searchTerms[i]);
              if (idx === 0 || (idx > 0 && !prefixOnly)) {
                found = true;
                searchTerms.splice(i, 1);
                break;
              }
            }
            //if expression not found we look for next one
            if (!found) i++;
          }
        }
      }
      return searchTerms.length === 0;
    };
        //if (select != null && select != undefined) {
        //    return value.filter(item => item.name.startWith(select));
        //}
        //return value;
  
}
