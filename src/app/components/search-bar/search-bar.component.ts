import { Component, OnInit, Input } from '@angular/core';
import { DataService } from "../../services/data.service";
import { Observable } from "rxjs/Observable";
import { ViewContextService, ViewContext } from '../../services/viewcontext.service';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
  model: any;
  viewContext: ViewContext;
  // @Input() private allLists: any;

  listMosadot: Observable<any>;
  listRashuyot: Observable<any>;
  listMechozot: Observable<any>;
  list: Observable<any>;
  constructor(private dataService: DataService, public context: ViewContextService, private router: Router) { }

  ngOnInit() {
    this.dataService.getLists().subscribe(result => {
      this.list = result.infoLists;
    });
 
  }

  search = (text$: Observable<string>) =>
    text$
      //.merge(this.focus$)
      //.merge(this.click$.filter(() => !this.instance.isPopupOpen()))
      .map(term => term.length < 2 ? []
        : this.list.filter(a => this.filterfortypeahead(term, a, [
          { key: "Semel", pos: 1 },
          { key: "Shem", pos: 0 },
          { key: "Address", pos: 0 },
          { key: "Mode", pos: 0 }
        ]) === true));
  //.slice(0, 10))

  filterfortypeahead(val, actualState, props) {

    var tempVal = val.replace(/[(|)|,]/g, " ");
    var searchTerms = tempVal.split(" ");
    if (typeof actualState === 'object') {
      for (var p = 0; p < props.length; p++) { //prop
        var key = props[p].key;
        var prefixOnly = (props[p].pos === 0);
        var i = 0;
        while (i < searchTerms.length) { //expr
          if (actualState[key] != null) {
            var words = actualState[key].toString().split(' ');
            var found = false;
            for (var j = 0; j < words.length; j++) {
              var idx = words[j].indexOf(searchTerms[i]);
              if (idx === 0 || (idx > 0 && !prefixOnly)) {
                found = true;
                searchTerms.splice(i, 1);
                break;
              }
            }
          }
          //if expression not found we look for next one
          if (!found) i++;
        }
      }
    }

    return searchTerms.length === 0;
  };
  formatter = (x: { Shem: string, Semel: number, Address: string, Mode: string }) => x.Shem + "(" + x.Semel.toString() + ")" + (x.Address ? ","+ x.Address:" ");

  convertMode(mode: number): string {
    return eMode[mode];
  }
  searchChange(): void {
    this.router.navigate([this.model.Mode]);
    this.context.viewContext$.subscribe(viewContext => this.viewContext = viewContext);
    this.viewContext.mode = this.model.Mode;
    this.viewContext.semel = this.model.Semel;
    this.context.changeViewContext(this.viewContext);
   
  }
}

export enum eMode {
  "ארצי",
  "מחוז",
  "רשות",
  "מוסד"
}
