
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { DataService } from './services/data.service';
import { ViewContextService} from './services/viewcontext.service';
import { InMemoryService, InMemoryServiceConfig  } from './services/mocks/in-memory.service';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HeaderComponent } from './components/header/header.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { HttpInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { TypeaheadFilterPipe } from './pipes/typeahead-filter.pipe';


import { GeneralModule } from './general/general.module';



import { RouterModule, Routes ,NoPreloading} from '@angular/router';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

import { HeaderIturMosadComponent } from './components/header-itur-mosad/header-itur-mosad.component';
import { HeaderIturCityComponent } from './components/header-itur-city/header-itur-city.component';
import { HeaderIturDistrictComponent } from './components/header-itur-district/header-itur-district.component';





const appRoutes: Routes = [ 
  //{ path: 'school/:id', component: HeroDetailComponent },
  //{ path: 'rashut/:id', component: HeroDetailComponent },
 
  { path: 'school', loadChildren: './mosad/mosad.module#MosadModule', data: { title: 'מוסד' } },
 { path: 'school/:id', loadChildren: './mosad/mosad.module#MosadModule', data: { title: 'מוסד' } },
  { path: 'national', loadChildren: './national/national.module#NationalModule', data: { title: 'ארצי' } },
  { path: 'city', loadChildren: './rashut/rashut.module#RashutModule', data: { title: 'רשות' } },
  { path: 'city/:id', loadChildren: './rashut/rashut.module#RashutModule', data: { title: 'רשות' } },
  { path: 'district', loadChildren: './machoz/machoz.module#MachozModule', data: { title: 'מחוז' } },
  { path: 'district/:id', loadChildren: './machoz/machoz.module#MachozModule', data: { title: 'מחוז' } },
  { path: '',redirectTo: 'national',pathMatch: 'full'},
  { path: '**', component: PageNotFoundComponent }
];

var importedModules: Array<any> = [
  BrowserModule,
  FormsModule,
  HttpModule, 
  RouterModule.forRoot(appRoutes, { preloadingStrategy: NoPreloading }),
  GeneralModule
];

//if (process.env.ENV === 'dev') {
  console.log('Enabling mocked services.');
  importedModules.push( HttpInMemoryWebApiModule.forRoot(InMemoryService, InMemoryServiceConfig),   );
//}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SearchBarComponent,
    TypeaheadFilterPipe,
    PageNotFoundComponent,
    HeaderIturMosadComponent,
    HeaderIturCityComponent,
   HeaderIturDistrictComponent,
  ],
  imports: importedModules,
  providers: [
    { provide: 'BASE_URL', useFactory: getBaseUrl },
    DataService,
    ViewContextService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href;
}
